# HOLD

"DESCRIPTION HERE"

## Build for Android
#### 1. Create a keystore
On Mac/Linux, use the following command:
```
keytool -genkey -v -keystore ~/key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias key
```
On Windows, use the following command:
```
keytool -genkey -v -keystore c:/Users/USER_NAME/key.jks -storetype JKS -keyalg RSA -keysize 2048 -validity 10000 -alias key
```

#### 2. Reference the keystore from the app
Edit a file named `HOLD/android/key.properties` that contains a reference to your keystore:
```
storePassword=<password from previous step>
keyPassword=<password from previous step>
keyAlias=key
storeFile=<location of the key store file, such as /Users/<user name>/key.jks>
```

#### 3. Building the app for release
From the command line:
1. Enter cd `<app dir>`
(Replace `<app dir>` with your application’s directory.)
2. Run `flutter build appbundle`